#!/bin/bash

mv -f mailbox_import.php /usr/local/ispconfig/interface/web/tools/
mv -f mailbox_import.htm /usr/local/ispconfig/interface/web/tools/templates/
mv -f mailbox_import.menu.php /usr/local/ispconfig/interface/web/tools/lib/menu.d/

chown ispconfig:ispconfig /usr/local/ispconfig/interface/web/tools/mailbox_import.php
chown ispconfig:ispconfig /usr/local/ispconfig/interface/web/tools/templates/mailbox_import.htm
chown ispconfig:ispconfig /usr/local/ispconfig/interface/web/tools/lib/menu.d/mailbox_import.menu.php